function calcular(){
    
    let parrafo = document.getElementById('parrafoEnganche');
    let constante = 0.3;
    let valorAuto = document.getElementById('txtValor').value;
    let enganche = valorAuto * constante;
    let interes = document.getElementById('cmbPlanes').value;

    
    parrafo.innerHTML = "$" + enganche;

    let parrafo1 = document.getElementById('parrafoFinanciar');
    let financiar = (valorAuto - enganche) + ((valorAuto - enganche) * document.getElementById('cmbPlanes').value);
    parrafo1.innerHTML = "$" + financiar;

    let parrafo2 = document.getElementById('parrafoPagoMensual');

    if(interes == 0.125){
        let pagoMensual = financiar / 12;
        parrafo2.innerHTML = "$" + pagoMensual;
    }else if(interes == 0.172){
        let pagoMensual = financiar / 18;
        parrafo2.innerHTML = "$" + pagoMensual;
    }else if(interes == 0.21){
        let pagoMensual = financiar /24;
        parrafo2.innerHTML = "$" + pagoMensual;
    }else if(interes == 0.26){
        let pagoMensual = financiar / 36;
        parrafo2.innerHTML = "$" + pagoMensual;
    }else if(interes == 0.45){
        let pagoMensual = financiar / 48;
        parrafo2.innerHTML = "$" + pagoMensual;
    }
    


}

function limpiar(){
    let parrafo = document.getElementById('parrafoEnganche');
    parrafo.innerHTML = "";
    let parrafo1 = document.getElementById('parrafoFinanciar');
    parrafo1.innerHTML = "";
    let parrafo2 = document.getElementById('parrafoPagoMensual');
    parrafo2.innerHTML = "";
}
